/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parlem.to;

import java.io.Serializable;

/**
 *
 * @author David
 */
public class TOProducte implements Serializable{
    private long id;
    private String productName;
    private String producteTypeName;
    private long numeracioTerminal;
    private String soldAt;
    private String costumerId;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the producteTypeName
     */
    public String getProducteTypeName() {
        return producteTypeName;
    }

    /**
     * @param producteTypeName the producteTypeName to set
     */
    public void setProducteTypeName(String producteTypeName) {
        this.producteTypeName = producteTypeName;
    }

    /**
     * @return the numeracioTerminal
     */
    public long getNumeracioTerminal() {
        return numeracioTerminal;
    }

    /**
     * @param numeracioTerminal the numeracioTerminal to set
     */
    public void setNumeracioTerminal(long numeracioTerminal) {
        this.numeracioTerminal = numeracioTerminal;
    }

    /**
     * @return the soldAt
     */
    public String getSoldAt() {
        return soldAt;
    }

    /**
     * @param soldAt the soldAt to set
     */
    public void setSoldAt(String soldAt) {
        this.soldAt = soldAt;
    }

    /**
     * @return the costumerId
     */
    public String getCostumerId() {
        return costumerId;
    }

    /**
     * @param costumerId the costumerId to set
     */
    public void setCostumerId(String costumerId) {
        this.costumerId = costumerId;
    }
    
    
    
}
