/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parlem.tabs;

import com.parlem.to.TOProducte;
import com.parlem.web.ClientBean;
import com.parlem.web.common.ACTInsertUpdateShow;
import com.parlemb.api.APIParlem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author David
 */
@ManagedBean
@ViewScoped
public class ProductesBean extends ACTInsertUpdateShow implements Serializable {

    private Collection<TOProducte> productes = null;
    private APIParlem parlem = null;

    /**
     *
     * Inicialitza tots els productes del client.
     *
     */
    @PostConstruct
    public void init() {
        showObject();
    }

    /**
     *
     * Mostra els productes.
     *
     */
    @Override
    public void showObject() {
        this.productes = null;
        this.parlem = new APIParlem();

        String jsonProuctes = this.parlem.getProductes();
        JSONObject myjson = new JSONObject(jsonProuctes);
        JSONArray jsonArray = myjson.getJSONArray("productes");
        FacesContext context = FacesContext.getCurrentInstance();
        ClientBean client = (ClientBean) context.getApplication().evaluateExpressionGet(context, "#{clientBean}", ClientBean.class);

        int size = jsonArray.length();
        ArrayList<TOProducte> arrProductes = new ArrayList();
        for (int i = 0; i < size; i++) {
            JSONObject producteJson = jsonArray.getJSONObject(i);
            TOProducte producte = new TOProducte();
            producte.setId(producteJson.getLong("_id"));
            producte.setProductName(producteJson.getString("productName"));
            producte.setProducteTypeName(producteJson.getString("productTypeName"));
            producte.setNumeracioTerminal(producteJson.getLong("numeracioTerminal"));
            producte.setSoldAt(producteJson.getString("soldAt"));
            producte.setCostumerId(producteJson.getString("customerId"));

            if(client.getClient().getCustomerId().equals(producte.getCostumerId())){
                arrProductes.add(producte);
            }
            
        }

        this.productes = new ArrayList(arrProductes);

    }

    /**
     * @return the productes
     */
    public Collection<TOProducte> getProductes() {
        return productes;
    }

    /**
     * @param productes the productes to set
     */
    public void setProductes(Collection<TOProducte> productes) {
        this.productes = productes;
    }

    /**
     * @return the parlem
     */
    public APIParlem getParlem() {
        return parlem;
    }

    /**
     * @param parlem the parlem to set
     */
    public void setParlem(APIParlem parlem) {
        this.parlem = parlem;
    }

}
