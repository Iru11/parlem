/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parlem.web;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author David
 */
@ManagedBean
@ApplicationScoped
public class AppBean {
    
    private String nameApp;
    private String titleApp;
    
    /**
     * Creates a new instance of AppBean
     */
    public AppBean() {
        this.nameApp = "Parlem";
        this.titleApp = "Dades client Parlem";
    }

    /**
     * 
     * Retorna el nom de la aplicación.
     * 
     * @return Nom aplicació.
     */
    public String getNameApp() {
        return nameApp;
    }

    /**
     * 
     * Estableix el nom de la aplicació.
     * 
     * @param nameApp Nom de la aplicació.
     */
    public void setNameApp(String nameApp) {
        this.nameApp = nameApp;
    }

    /**
     * 
     * Retorna el títol de la aplicació.
     * 
     * @return Títol de la aplicació.
     */
    public String getTitleApp() {
        return titleApp;
    }

    /**
     * 
     * Estableix el títol de aplicació.
     * 
     * @param title títol de la aplació.
     */
    public void setTitleApp(String title) {
        this.titleApp = title;
    }
    
}
