/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parlem.web.common;

/**
 *
 * Interface para insertar, actualitzar, mostrar 
 * 
 * @author David Iruela
 */
public interface ITFInsertUpdateShow {           
    
    /**
     * 
     * Inicialitza un nou objecte
     * 
     */
    public void newObject();
    
    /**
     * 
     * Inicialitza un objecte existent
     * 
     * @param object Objecte
     */
    public void existObject(Object object);
    
    /**
     * 
     * Inserta o actualitza un objecte
     * 
     * @param object Objecte
     */
    public void insertUpdate(Object object);
       
    
    /**
     * 
     * Mostra un objecte
     * 
     */
    public void showObject();
    
    
}
