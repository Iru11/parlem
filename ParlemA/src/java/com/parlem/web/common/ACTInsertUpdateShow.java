/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parlem.web.common;

/**
 *
 * Clase abstracta per insertar, actualitzar, mostrar un objecte
 * 
 * @author David Iruela
 */
public abstract class ACTInsertUpdateShow implements ITFInsertUpdateShow{
    
    @Override
    public void newObject() {
        
    }

    @Override
    public void existObject(Object object) {
        
    }

    @Override
    public void insertUpdate(Object object) {
        
    }      

    @Override
    public void showObject() {
        
    }
    
}
