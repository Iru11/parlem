/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parlem.web;

import com.parlem.tabs.ProductesBean;
import com.parlem.to.TOClient;
import com.parlemb.api.APIParlem;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.json.JSONObject;
import org.primefaces.PrimeFaces;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author David
 */
@ManagedBean
@SessionScoped
public class ClientBean implements Serializable {

    private TOClient client;
    private APIParlem parlem;

    /**
     *
     * Inicia el client.
     */
    @PostConstruct
    public void init() {
        client = new TOClient();
        parlem = new APIParlem();
        
        JSONObject clientJson = new JSONObject(parlem.getClient());
        
        client.setId(clientJson.getLong("_id"));
        client.setDocType(clientJson.getString("docType"));
        client.setDocNum(clientJson.getString("docNum"));
        client.setEmail(clientJson.getString("email"));
        client.setCustomerId(clientJson.getString("customerId"));
        client.setGivenName(clientJson.getString("givenName"));
        client.setFamilyName1(clientJson.getString("familyName1"));
        client.setPhone(clientJson.getString("phone"));
    }

    /**
     * @return the client
     */
    public TOClient getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(TOClient client) {
        this.client = client;
    }

    /**
     *
     * Captura e evento cuando el usuario cambia de tab.
     *
     * @param event Evento de cambira de tab.
     */
    public void onTabChange(TabChangeEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (event.getTab() != null) {
            switch (event.getTab().getId()) {
                case "tabProductesContractats":
                    ProductesBean productes = (ProductesBean) context.getApplication().evaluateExpressionGet(context, "#{movimientoBean}", ProductesBean.class);
                    productes.showObject();
                    PrimeFaces.current().ajax().update("tabs:pnlProductes");
                    break;
                default:
                    break;
            }

        }
    }

}
