/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parlemb.api;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;

/**
 *
 * Interface amb els webservices.
 *
 * @author David
 */
public class APIParlem {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8084/ParlemA/";

    /**
     *
     * Constructor.
     *
     */
    public APIParlem() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("api");
    }

    /**
     *
     * Retorna json amb les dades del client a través de la api
     *
     * @return json del client
     */
    public String getClient() {
        String jsonClient = null;
        try {
            WebTarget resource = webTarget;
            resource = resource.path("client");
            jsonClient = resource.request(MediaType.APPLICATION_JSON).get(String.class);
            
        } catch (ClientErrorException ex) {
            ex.getMessage();
        }
        return jsonClient;
    }

    /**
     *
     * Realitza un post a la api i retorna el nou json del client
     *
     * @return json del client
     */
    public String postClientApi() {
        String jsonClient = null;
        try {
            WebTarget resource = webTarget;
            resource = resource.path("postClient");

            JSONObject jsonObject = new JSONObject();
            String json;

            //Les dades s'obtindrien de una base de dades (aixó es un exemple)
            //Com seria si s'obtingues de la base de dades:
            //Client client = clientDb.getAll(); --> Objecte que recull totes les dades de una base de dades per exemple
            //jsonObject.put("_id", client.getId());
            //jsonObject.put("_id", client.getDocType());
            jsonObject.put("_id", "55555");
            jsonObject.put("docType", "nif");
            jsonObject.put("docNum", "11223344E");
            jsonObject.put("email", "it@parlem.com");
            jsonObject.put("customerId", "11111");
            jsonObject.put("givenName", "Enriqueta");
            jsonObject.put("familyName1", "Parlem");
            jsonObject.put("phone", "668668668");
            json = jsonObject.toString();

            if (json != null && !json.trim().equals("")) {
                jsonClient = resource.request().post(Entity.json(json), String.class);
            }

        } catch (ClientErrorException ex) {
            ex.getMessage();
        }
        return jsonClient;
    }
    
     /**
     *
     * Retorna json amb les dades dels productes a través de la api
     *
     * @return json del productes
     */
    public String getProductes() {
        String jsonProductes = null;
        try {
            WebTarget resource = webTarget;
            resource = resource.path("productes");
            jsonProductes = resource.request(MediaType.APPLICATION_JSON).get(String.class);
            
        } catch (ClientErrorException ex) {
            ex.getMessage();
        }
        return jsonProductes;
    }

    /**
     *
     * Tanca la sessió.
     *
     */
    public void close() {
        client.close();
    }

}
